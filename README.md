This is a barebones playground to test my ideas for a simple entity component system in C++.
There's not much focus on speed or safety at the moment, but it is something that I will address later.

Current features:

- Create a World (scene) with entities
- Registering any type as a component with automatic storage creation and allocation
- Add components to an entity
- Get a pointer to a component for an entity