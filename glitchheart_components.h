#ifndef GH_COMPONENTS_H
#define GH_COMPONENTS_H

#include <typeinfo>
#include <stdio.h>
#include <stdlib.h>

#include "hashtable.h"

static size_t type_id_hashing_function(size_t key)
{
    // Just return the type_id, since that is already the hash
    return key;
}

// The max amount of component an entity can have
#define MAX_COMPONENTS_FOR_ENTITY 32

struct ComponentHandle
{
    int handle;
};

struct EntityHandle
{
    int handle;
};

// The entity struct has no real data. Only the hashes and handles of the types it contains.
struct Entity
{
    hash_table::HashTable<size_t, ComponentHandle> component_hashes;
};

// The same as a scene. Container of all entities and the storages
struct World
{
    Entity *entities;
    int entity_count;

    int current_entity_max;

    hash_table::HashTable<size_t, void*> storages;
};

template <typename ComponentType>
struct Storage
{
    ComponentType *components;
    int count;

    static void alloc(Storage *storage, size_t max_count)
    {
        storage->components = (ComponentType *)malloc(sizeof(ComponentType) * max_count);
    }

    static ComponentType *get_component_by_handle(Storage *storage)
    {
        return &storage->components[0];
    }

    static ComponentType *get_component(ComponentHandle handle, Storage<ComponentType> *storage)
    {
        return &storage->components[handle.handle - 1];
    }
};

template <typename ComponentType>
static void register_component(World *world)
{
    size_t hash = typeid(ComponentType).hash_code();

    if(hash_table::has_key(&world->storages, hash))
        return; // the component has been registered already

    auto *ptr = (Storage<ComponentType>*)malloc(sizeof(Storage<ComponentType>));
    Storage<ComponentType>::alloc(ptr, world->current_entity_max);
    
    hash_table::add(&world->storages, hash, (void*)ptr);
}

template <typename ComponentType>
static Storage<ComponentType> * get_storage(World *world)
{
    size_t hash = typeid(ComponentType).hash_code();

    if(!hash_table::has_key(&world->storages, hash))
        return nullptr;
        
    // @Incomplete: Error checking
    return (Storage<ComponentType>*)hash_table::get(&world->storages, hash);
}

template <typename ComponentType>
static ComponentType * get_component(EntityHandle entity, World *world)
{
    Entity *e = &world->entities[entity.handle - 1];

    if(auto *storage = get_storage<ComponentType>(world))
    {
        size_t hash = typeid(ComponentType).hash_code();
        if(!hash_table::has_key(&e->component_hashes, hash))
            return nullptr;

        ComponentHandle handle = hash_table::get(&e->component_hashes, hash);
        return Storage<ComponentType>::get_component(handle, storage);
    }

    return nullptr;
}

template <typename ComponentType>
static ComponentType * add_component(const ComponentType &value, EntityHandle entity, World *world, bool auto_storage_creation = true)
{
    if(auto *storage = get_storage<ComponentType>(world))
    {
        if(auto *existing = get_component<ComponentType>(entity, world))
        {
            // If the component already exists just replace the value.
            *existing = value;
            return existing;
        }

        storage->components[storage->count++] = value;
        Entity *e = &world->entities[entity.handle - 1];
        hash_table::add(&e->component_hashes, typeid(ComponentType).hash_code(), { storage->count });

        return &storage->components[storage->count - 1];
    }
    else if(auto_storage_creation) // If the storage doesn't exist, create it
    {
        register_component<ComponentType>(world);
        return add_component<ComponentType>(value, entity, world);
    }

    return nullptr;
}

namespace game
{
    struct Vector3f
    {
        float x;
        float y;
        float z;
    };

    struct Transform
    {
        Vector3f position;
        Vector3f euler_angles;
        Vector3f scale;
    };

    struct MeshRenderer
    {
        int mesh_id;
    };
}

static void init_world(World *world, int entity_count)
{
    world->entities = (Entity*)malloc(sizeof(Entity) * entity_count);
    world->current_entity_max = entity_count;
    world->entity_count = 0;
    hash_table::init(&world->storages, MAX_COMPONENTS_FOR_ENTITY, &type_id_hashing_function);
}

static EntityHandle register_entity(World *world)
{
    Entity &e = world->entities[world->entity_count++];

    hash_table::init(&e.component_hashes, MAX_COMPONENTS_FOR_ENTITY, &type_id_hashing_function);

    return { world->entity_count };
}

/* THIS IS JUST PSEUDO-ISH CODE FOR REGISTERING INSPECTOR RENDERING CALLBACKS */
// namespace editor
// {
//     typedef void (*RenderInspector)(void *component);

//     struct InspectorCallbackEntry
//     {
//         size_t type_hash;
//         RenderInspector callback;
//     };

//     struct EditorCallbacks
//     {
//         InspectorCallbackEntry inspector_callbacks[MAX_COMPONENTS_FOR_ENTITY];
//         int inspector_callback_count;
//     };

//     template<typename ComponentType>
//     static void register_inspector_callback_for_type(RenderInspector callback, EditorCallbacks *editor_callbacks)
//     {
//         size_t hash = type_id(ComponentType).hash_code();
//         // Code for adding the callback
//     }
// };
#endif