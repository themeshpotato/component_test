#include "glitchheart_components.h"
#include "hashtable.h"

size_t hash_function(size_t key)
{
    return key;
}

int main()
{
    // hash_table::HashTable<size_t, int> hash_table;

    // if(hash_table::init(&hash_table, 20, &hash_function) != hash_table::HASH_TABLE_INITIALIZED)
    //     return 1;

    // hash_table::add(&hash_table, (size_t)20, 30);
    // hash_table::add(&hash_table, (size_t)10, 1);
    // hash_table::add(&hash_table, (size_t)200, 3);
    // hash_table::add(&hash_table, (size_t)4000, 10);
    // hash_table::add(&hash_table, (size_t)5, 0);

    // {
    //     auto val = hash_table::get(&hash_table, (size_t)20);
    //     printf("Found value %d\n", val);
    // }

    // {
    //     auto val = hash_table::get(&hash_table, (size_t)200);
    //     printf("Found value %d\n", val);
    // }
    

    // Create the world and register the used component types
    World world;
    init_world(&world, 2048);
    register_component<game::Transform>(&world); // We don't register the MeshRenderer-component, since it will automatically create a storage when we add the first component.

    // Create the entities
    EntityHandle entity = register_entity(&world);
    EntityHandle entity2 = register_entity(&world);

    // Set the first entity's component values and add them to the entity.
    game::Transform new_transform;
    new_transform.position.x = 1.0f;
    new_transform.position.y = 2.0f;
    new_transform.position.z = 3.0f;
    add_component<game::Transform>(new_transform, entity, &world);
    game::MeshRenderer renderer;
    renderer.mesh_id = 1002;
    add_component<game::MeshRenderer>(renderer, entity, &world);

    // Set the second entity's component values and add them to the entity.
    new_transform.position.x = 20.0f;
    new_transform.position.y = -20.0f;
    new_transform.position.z = 0.0f;
    renderer.mesh_id = 1003;
    add_component<game::Transform>(new_transform, entity2, &world);
    add_component<game::MeshRenderer>(renderer, entity2, &world);

    // Get some of the values and print them
    game::Transform *transform = get_component<game::Transform>(entity, &world);
    game::MeshRenderer *mesh_renderer = get_component<game::MeshRenderer>(entity2, &world);

    printf("Entity 1 -> Transform -> position: %f, %f, %f\n", transform->position.x, transform->position.y, transform->position.z);
    printf("Entity 2 -> MeshRenderer -> mesh_id: %d\n", mesh_renderer->mesh_id);

    return 0;
}