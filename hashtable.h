#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <cstddef>
#include <cassert>
#include <cstring>

namespace hash_table
{
    template <typename KeyType, typename ValueType>
struct Entry
{
    KeyType key;
    ValueType value;
    size_t hash_code;
};

template <typename KeyType, typename ValueType>
struct Bucket
{
    Entry<KeyType, ValueType> *entries;
    int count;
    int size;
};

template <typename KeyType, typename ValueType>
struct HashTable
{
    Bucket<KeyType, ValueType> *buckets;
    int size;
    size_t (*hash_function)(KeyType key);
};

enum HashTableInitializationResult
{
    HASH_TABLE_INITIALIZED,
    HASH_TABLE_MISSING_TABLE_PTR,
    HASH_TABLE_MISSING_HASH_FUNCTION_PTR,
    HASH_TABLE_NOT_ENOUGH_MEMORY
};

template <typename KeyType, typename ValueType>
static HashTableInitializationResult init(HashTable<KeyType, ValueType> * hash_table, size_t size, size_t (*hash_function)(KeyType key))
{
    if(!hash_table)
        return HASH_TABLE_MISSING_TABLE_PTR;
    else if(!hash_function)
        return HASH_TABLE_MISSING_HASH_FUNCTION_PTR;
        
    hash_table->size = size;
    hash_table->hash_function = hash_function;
    hash_table->buckets = (Bucket<KeyType, ValueType>*)malloc(sizeof(Bucket<KeyType, ValueType>) * size);

    if(!hash_table->buckets)
        return HASH_TABLE_NOT_ENOUGH_MEMORY;

    return HASH_TABLE_INITIALIZED;
};

size_t _get_hash_table_position(size_t hash, size_t size)
{
    if(hash < 0)
        return -(hash % size);
    return hash % size;
}

template <typename KeyType, typename ValueType>
static bool has_key(HashTable<KeyType, ValueType> *hash_table, KeyType key)
{
    size_t hash = hash_table->hash_function(key);
    size_t position = _get_hash_table_position(hash, hash_table->size);

    auto *bucket = &hash_table->buckets[position];
    
    bool found = false;

    for(int i = 0; i < bucket->count; i++)
    {
        if(bucket->entries[i].hash_code == hash)
        {
            found = true;
            break;
        }
    }

    return found;
}

template <typename KeyType, typename ValueType>
static ValueType get(HashTable<KeyType, ValueType> *hash_table, KeyType key)
{
    ValueType result;

    size_t hash = hash_table->hash_function(key);
    size_t position = _get_hash_table_position(hash, hash_table->size);

    auto *bucket = &hash_table->buckets[position];
    
    bool found = false;

    for(int i = 0; i < bucket->count; i++)
    {
        if(bucket->entries[i].hash_code == hash)
        {
            found = true;
            result = bucket->entries[i].value;
            break;
        }
    }

    assert(found);

    return result;
}

template <typename KeyType, typename ValueType>
static void add(HashTable<KeyType, ValueType> *hash_table, KeyType key, ValueType value)
{
    size_t hash = hash_table->hash_function(key);
    size_t position = _get_hash_table_position(hash, hash_table->size);

    auto *bucket = &hash_table->buckets[position];
    
    if(!bucket->entries)
    {
        bucket->entries = (Entry<KeyType, ValueType>*)malloc(sizeof(Entry<KeyType, ValueType>));
        bucket->size = 1;
    }

    if(bucket->count == bucket->size)
    {
        auto *new_ptr = (Entry<KeyType, ValueType>*)malloc(sizeof(Entry<KeyType, ValueType>) * bucket->size * 2);
        memcpy(new_ptr, bucket->entries, sizeof(Entry<KeyType, ValueType>) * bucket->size);
        free(bucket->entries);
        bucket->entries = new_ptr;

        bucket->size *= 2;
    }

    Entry<KeyType, ValueType> new_entry;
    new_entry.hash_code = hash;
    new_entry.key = key;
    new_entry.value = value;

    bucket->entries[bucket->count++] = new_entry;
}
}



#endif